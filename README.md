# Getting Started

##Launch the app:
```
click on AppEmpleadosApplication and launch the app
```

# Docker Database

##Launching MySQL using Docker:
```
docker run -d -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=employee -e MYSQL_USER=employee-user -e MYSQL_PASSWORD=password -p 3306:3306 mysql:5.7
```

##Launching MySQL using Docker but creating a volume: 
```
docker run -d -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=employee -e MYSQL_USER=employee-user -e MYSQL_PASSWORD=password -p 3306:3306 --volume mysql-db-employee:/var/lib/mysql mysql:5.7
```

##Connect to the MySQL docker database from a MySQL Shell

###You need to do the connection: 
```
\connect employee-user@localhost:3306
```

###Then use the sql mode: 
```
\sql
```

###Then use the employee schema: 
```
use employee;
```

###After that, you can test if the connection is ok: 
```
select * from employee;
```