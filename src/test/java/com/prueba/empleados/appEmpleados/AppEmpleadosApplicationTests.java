package com.prueba.empleados.appEmpleados;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.prueba.empleados.appEmpleados.entity.Employee;
import com.prueba.empleados.appEmpleados.repository.EmployeeRepository;

@SpringBootTest
class AppEmpleadosApplicationTests {
	
	@Autowired
	EmployeeRepository repository;

	@Test
	public void createEmployee() {
		
		Employee e = new Employee("Ander Herrera", "Desarrollador Fullstack");
		
		Employee aux = repository.save(e);
		
		assertTrue(aux.getFullname().equals("Ander Herrera"));
	}
	
	@Test
	public void findById() {
		Optional<Employee> emp =  repository.findById((int) 1);
		assertTrue(emp.isPresent());
	}
	
	@Test
	public void findByfullname() {
		Employee emp =  repository.findByFullname("Ander Herrera");
		assertTrue(emp.getFullname().equals("Ander Herrera"));
	}

}
