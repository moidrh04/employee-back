package com.prueba.empleados.appEmpleados.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.empleados.appEmpleados.entity.Employee;
import com.prueba.empleados.appEmpleados.repository.EmployeeRepository;

@Service
public class EmployeeService {
	
	@Autowired
	EmployeeRepository repository;
	
	public List<Employee> getAllEmployees() {
		List<Employee> empls = repository.findAll();
		
		return empls;
	}

	public Employee getAnEmployeebyFullname(String fullname) {
		
		Employee emp = repository.findByFullname(fullname);
		
		return emp;
	}

	public int createEmp(String fullname, String employeefunct) {
		
		boolean exists = repository.existsByFullname(fullname);
		
		if(exists == true) {
			Employee e = repository.findByFullname(fullname);
			
			e.setFunction(employeefunct);
			
			return repository.save(e).getId();
		}
		
		Employee e = new Employee(fullname, employeefunct);
		
		int empId = repository.save(e).getId();
		
		return empId;
	}

}
