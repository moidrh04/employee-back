package com.prueba.empleados.appEmpleados.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String fullname;
	
	private String employee_function;
	
	protected Employee() {}
	
	public Employee(String fullname, String function) {
		this.fullname = fullname;
		this.employee_function = function;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getFunction() {
		return employee_function;
	}

	public void setFunction(String function) {
		this.employee_function = function;
	}

	public int getId() {
		return id;
	}
	
	

}
