package com.prueba.empleados.appEmpleados.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prueba.empleados.appEmpleados.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	
	Employee findByFullname(String fullname);
	
	boolean existsByFullname(String fullname);

}
